
#**************************************************
# Honor Code: This work is mine unless otherwise cited.
# Zach Dickinson
# BIO/CS 300
# Lab 4
# Date: 2-17
#
# Purpose: to create a DNA reader that looks for and records the number of ATG's.
#**************************************************

my_file = open('Sequence.txt')
count = 0
name = ""
sequence = ""
codons = ""
seqList = []
codList = []

for line in my_file:
	if count%2==0:
		name = line[1:-1]
		print "Name: ", name
	else:
		sequence = line.replace(" ", "")
		sequence = sequence.replace("\n","")
		sequence = sequence.upper()
		print "Sequence: ", sequence
		seqList.append(sequence)
	count +=1

print "List: ", seqList

my_file.close()

codes = dict(GCA='A', GCC='A', GCG='A',GCT='A',
             AGA='R', AGG='R', CGA='R', CGC='R', CGG='R', CGT='R',
             AAT='N', AAC='N',
             GAC='D', GAT='D',
             TGC='C', TGT='C',
             GAA='E', GAG='E',
             CAA='Q', CAG='Q',
             GGA='G', GGC='G', GGG='G', GGT='G',
             CAC='H', CAT='H',
             ATA='I', ATC='I', ATT='I',
             CTA='L', CTC='L', CTG='L', CTT= 'L', TTA='L', TTG='L',
             AAA='K', AAG='K', ATG='M',
             TTC='F', TTT='F',
             CCA='P', CCC='P', CCG='P', CCT='P',
             AGC='S', AGT='S', TCA='S', TCC='S', TCG= 'S', TCT='S',
             ACA='T', ACC='T', ACG='T', ACT='T',
             TGG='W',
             TAC='Y', TAT='Y', GTA='V', GTC='V', GTG='V', GTT='V',
             TAA='-', TAG='-', TGA='-')


for i in range(0, len(sequence), 3):
	codons += codes[sequence[i:i+3]]
	codList.append(codons)

start = set('ATG')
for x in codList:
	if start & set(sequence):
		print x



print "Methionine Occurences: ", codList








