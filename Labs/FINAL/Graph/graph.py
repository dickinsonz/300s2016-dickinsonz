from Bio.Blast import NCBIWWW # import the module related to BLAST
from Bio import SeqIO
import matplotlib.pyplot as plt
import numpy as np

my_file = open('sequences.fasta')
count = 0
name = ""
percent = ""
seqList=[]
animal = []

# parse through the whole file and save all seq in a list
for line in my_file:
	if count%2==0:
		name=line[1:len(line)-1]
		print "Name: ", name
		animal.append(name)
	else:
		percent = line.upper()
		print "Percent Similarity: ", percent
		seqList.append(percent)
	count+=1


my_file.close()

############ Needman-Wunsch ###########

#################################### HUMAN-CHIMP ##################################
# repeats 3 times, once for each type, Human-chimp, human-horse, and human-mouse.

infile2 = open('human.fasta', 'r')
infile1 = open('chimp.fasta', 'r')

seq1 = ""
infile1.readline()  
for line in infile1:
    line = line.replace('\n', '')
    seq1 = seq1 + line

seq2 = ""
infile2.readline()  
for line in infile2:
    line = line.replace('\n', '')
    seq2 = seq2 + line


seq1 = seq1.upper()
seq2 = seq2.upper()

# Scoring Matrix

N = len(seq1)
M = len(seq2)
gap = -1
mismatch = 0
match = 1
matrix = [[0 for i in range(M+1)] for j in range(N+1)]
for i in range(1, N+1):  
	if i == N:
		matrix[i][0] = matrix[i-1][0]
	else:
		matrix[i][0] = matrix[i-1][0] + gap
for i in range(1, M+1):  
	if i == M:
		matrix[0][i] = matrix[0][i-1]
	else:
		matrix[0][i] = matrix[0][i-1] + gap
for i in range(1, N+1):
    for j in range(1, M+1):
    	
        if seq1[i-1] == seq2[j-1]:
            score1 = matrix[i-1][j-1] + match
        else:
            score1 = matrix[i-1][j-1] + mismatch
        
        if j == N:
        	score2 = matrix[i][j-1]
        else:
        	score2 = matrix[i][j-1] + gap
      
        if i == M:
        	score3 = matrix[i-1][j]
        else:
        	score3 = matrix[i-1][j] + gap
        matrix[i][j] = max(score1, score2, score3)

# Matrix output
#print "Matrix"
#for row in matrix:
#    for elem in row:
#        print " {:>3} ".format(elem),
#    print ''

# Directional Strings

# function to determine directional string
def getDirectionalString(matrix, N, M):
    dstring = ''
    currentRow = N;
    currentCol = M;
    while (currentRow != 0 or currentCol != 0):
        if currentRow == 0:
            dstring = dstring + 'H'
            currentCol-=1
        elif currentCol == 0:
            dstring = dstring + 'V'
            currentRow-=1
        elif ((matrix[currentRow][currentCol-1] + gap)
            == (matrix[currentRow][currentCol])):
            dstring = dstring + 'H'
            currentCol-=1
        elif ((matrix[currentRow-1][currentCol] + gap)
              ==(matrix[currentRow][currentCol])):
            dstring = dstring + 'V'
            currentRow-=1
        else:
            dstring = dstring + 'D'
            currentRow-=1
            currentCol-=1
    return dstring     #end function

dstring = getDirectionalString(matrix, N, M) 
#print "Directional String: " + dstring

# Alignments Using Directional Strings
seq1pos = N - 1
seq2pos = M - 1
dirpos = 0;
alignSeq1 = ''
alignSeq2 = ''
matchLine = ''
matchCtr = 0
while (dirpos < len(dstring)):
    if (dstring[dirpos] == 'D'):
        alignSeq1 = seq1[seq1pos] + alignSeq1
        alignSeq2 = seq2[seq2pos] + alignSeq2
        if seq1[seq1pos] == seq2[seq2pos]:
            matchLine = '|' + matchLine
            matchCtr+=1
        else:
            matchLine = '.' + matchLine
        seq1pos-=1
        seq2pos-=1
    elif (dstring[dirpos] == 'V'):
        alignSeq1 = seq1[seq1pos] + alignSeq1
        alignSeq2 = '_' + alignSeq2
        seq1pos-=1
        matchLine = ' ' + matchLine
    else:
        alignSeq1 = '_' + alignSeq1
        alignSeq2 = seq2[seq2pos] + alignSeq2
        seq2pos-=1
        matchLine = ' ' + matchLine
    dirpos+=1

#print "Alignment:"
#perLine = 50
#numLines = len(alignSeq1) / perLine  # 50 to a line
#if len(alignSeq1) % perLine != 0:
#    numLines+=1

#for i in range(int(numLines)):
#    print str(i) + ' ',
#    print alignSeq1[i*perLine:(i+1)*perLine]
#    print str(i) + ' ',
#    print matchLine[i*perLine:(i+1)*perLine]
#    print str(i) + ' ',
#    print alignSeq2[i*perLine:(i+1)*perLine]
#print "\nAlignment Score: " + str(matrix[N][M])

if (len(seq1) > len(seq2)):
    print "\nMatch Percentage (Chimpanzee): " + str((matchCtr*1.0) / len(seq1))
else:
    print "\nMatch Percentage (Chimpanzee): " + str((matchCtr*1.0) / len(seq2))
    
    
################################# HUMAN-HORSE ###################################

infile2 = open('human.fasta', 'r')
infile1 = open('horse.fasta', 'r')

seq1 = ""
infile1.readline()  
for line in infile1:
    line = line.replace('\n', '')
    seq1 = seq1 + line

seq2 = ""
infile2.readline()  
for line in infile2:
    line = line.replace('\n', '')
    seq2 = seq2 + line


seq1 = seq1.upper()
seq2 = seq2.upper()

# Scoring Matrix

N = len(seq1)
M = len(seq2)
gap = -1
mismatch = 0
match = 1
matrix = [[0 for i in range(M+1)] for j in range(N+1)]
for i in range(1, N+1):  
	if i == N:
		matrix[i][0] = matrix[i-1][0]
	else:
		matrix[i][0] = matrix[i-1][0] + gap
for i in range(1, M+1):  
	if i == M:
		matrix[0][i] = matrix[0][i-1]
	else:
		matrix[0][i] = matrix[0][i-1] + gap
for i in range(1, N+1):
    for j in range(1, M+1):
    	
        if seq1[i-1] == seq2[j-1]:
            score1 = matrix[i-1][j-1] + match
        else:
            score1 = matrix[i-1][j-1] + mismatch
        
        if j == N:
        	score2 = matrix[i][j-1]
        else:
        	score2 = matrix[i][j-1] + gap
      
        if i == M:
        	score3 = matrix[i-1][j]
        else:
        	score3 = matrix[i-1][j] + gap
        matrix[i][j] = max(score1, score2, score3)

# Matrix output
#print "Matrix"
#for row in matrix:
#    for elem in row:
#        print " {:>3} ".format(elem),
#    print ''

# Directional Strings

# function to determine directional string
def getDirectionalString(matrix, N, M):
    dstring = ''
    currentRow = N;
    currentCol = M;
    while (currentRow != 0 or currentCol != 0):
        if currentRow == 0:
            dstring = dstring + 'H'
            currentCol-=1
        elif currentCol == 0:
            dstring = dstring + 'V'
            currentRow-=1
        elif ((matrix[currentRow][currentCol-1] + gap)
            == (matrix[currentRow][currentCol])):
            dstring = dstring + 'H'
            currentCol-=1
        elif ((matrix[currentRow-1][currentCol] + gap)
              ==(matrix[currentRow][currentCol])):
            dstring = dstring + 'V'
            currentRow-=1
        else:
            dstring = dstring + 'D'
            currentRow-=1
            currentCol-=1
    return dstring     #end function

dstring = getDirectionalString(matrix, N, M) 
#print "Directional String: " + dstring

# Alignments Using Directional Strings
seq1pos = N - 1
seq2pos = M - 1
dirpos = 0;
alignSeq1 = ''
alignSeq2 = ''
matchLine = ''
matchCtr = 0
while (dirpos < len(dstring)):
    if (dstring[dirpos] == 'D'):
        alignSeq1 = seq1[seq1pos] + alignSeq1
        alignSeq2 = seq2[seq2pos] + alignSeq2
        if seq1[seq1pos] == seq2[seq2pos]:
            matchLine = '|' + matchLine
            matchCtr+=1
        else:
            matchLine = '.' + matchLine
        seq1pos-=1
        seq2pos-=1
    elif (dstring[dirpos] == 'V'):
        alignSeq1 = seq1[seq1pos] + alignSeq1
        alignSeq2 = '_' + alignSeq2
        seq1pos-=1
        matchLine = ' ' + matchLine
    else:
        alignSeq1 = '_' + alignSeq1
        alignSeq2 = seq2[seq2pos] + alignSeq2
        seq2pos-=1
        matchLine = ' ' + matchLine
    dirpos+=1

#print "Alignment:"
#perLine = 50
#numLines = len(alignSeq1) / perLine  # 50 to a line
#if len(alignSeq1) % perLine != 0:
#    numLines+=1

#for i in range(int(numLines)):
#    print str(i) + ' ',
#    print alignSeq1[i*perLine:(i+1)*perLine]
#    print str(i) + ' ',
#    print matchLine[i*perLine:(i+1)*perLine]
#    print str(i) + ' ',
#    print alignSeq2[i*perLine:(i+1)*perLine]
#print "\nAlignment Score: " + str(matrix[N][M])

if (len(seq1) > len(seq2)):
    print "\nMatch Percentage (Horse): " + str((matchCtr*1.0) / len(seq1))
else:
    print "\nMatch Percentage (Horse): " + str((matchCtr*1.0) / len(seq2))
    
    
##################################### HUMAN-MOUSE ####################################### 


infile2 = open('human.fasta', 'r')
infile1 = open('mouse.fasta', 'r')

seq1 = ""
infile1.readline()  
for line in infile1:
    line = line.replace('\n', '')
    seq1 = seq1 + line

seq2 = ""
infile2.readline()  
for line in infile2:
    line = line.replace('\n', '')
    seq2 = seq2 + line


seq1 = seq1.upper()
seq2 = seq2.upper()

# Scoring Matrix

N = len(seq1)
M = len(seq2)
gap = -1
mismatch = 0
match = 1
matrix = [[0 for i in range(M+1)] for j in range(N+1)]
for i in range(1, N+1):  
	if i == N:
		matrix[i][0] = matrix[i-1][0]
	else:
		matrix[i][0] = matrix[i-1][0] + gap
for i in range(1, M+1):  
	if i == M:
		matrix[0][i] = matrix[0][i-1]
	else:
		matrix[0][i] = matrix[0][i-1] + gap
for i in range(1, N+1):
    for j in range(1, M+1):
    	
        if seq1[i-1] == seq2[j-1]:
            score1 = matrix[i-1][j-1] + match
        else:
            score1 = matrix[i-1][j-1] + mismatch
        
        if j == N:
        	score2 = matrix[i][j-1]
        else:
        	score2 = matrix[i][j-1] + gap
      
        if i == M:
        	score3 = matrix[i-1][j]
        else:
        	score3 = matrix[i-1][j] + gap
        matrix[i][j] = max(score1, score2, score3)

# Matrix output
#print "Matrix"
#for row in matrix:
#    for elem in row:
#        print " {:>3} ".format(elem),
#    print ''

# Directional Strings

# function to determine directional string
def getDirectionalString(matrix, N, M):
    dstring = ''
    currentRow = N;
    currentCol = M;
    while (currentRow != 0 or currentCol != 0):
        if currentRow == 0:
            dstring = dstring + 'H'
            currentCol-=1
        elif currentCol == 0:
            dstring = dstring + 'V'
            currentRow-=1
        elif ((matrix[currentRow][currentCol-1] + gap)
            == (matrix[currentRow][currentCol])):
            dstring = dstring + 'H'
            currentCol-=1
        elif ((matrix[currentRow-1][currentCol] + gap)
              ==(matrix[currentRow][currentCol])):
            dstring = dstring + 'V'
            currentRow-=1
        else:
            dstring = dstring + 'D'
            currentRow-=1
            currentCol-=1
    return dstring     #end function

dstring = getDirectionalString(matrix, N, M) 
#print "Directional String: " + dstring

# Alignments Using Directional Strings
seq1pos = N - 1
seq2pos = M - 1
dirpos = 0;
alignSeq1 = ''
alignSeq2 = ''
matchLine = ''
matchCtr = 0
while (dirpos < len(dstring)):
    if (dstring[dirpos] == 'D'):
        alignSeq1 = seq1[seq1pos] + alignSeq1
        alignSeq2 = seq2[seq2pos] + alignSeq2
        if seq1[seq1pos] == seq2[seq2pos]:
            matchLine = '|' + matchLine
            matchCtr+=1
        else:
            matchLine = '.' + matchLine
        seq1pos-=1
        seq2pos-=1
    elif (dstring[dirpos] == 'V'):
        alignSeq1 = seq1[seq1pos] + alignSeq1
        alignSeq2 = '_' + alignSeq2
        seq1pos-=1
        matchLine = ' ' + matchLine
    else:
        alignSeq1 = '_' + alignSeq1
        alignSeq2 = seq2[seq2pos] + alignSeq2
        seq2pos-=1
        matchLine = ' ' + matchLine
    dirpos+=1

#print "Alignment:"
#perLine = 50
#numLines = len(alignSeq1) / perLine  # 50 to a line
#if len(alignSeq1) % perLine != 0:
#    numLines+=1

#for i in range(int(numLines)):
#    print str(i) + ' ',
#    print alignSeq1[i*perLine:(i+1)*perLine]
#    print str(i) + ' ',
#    print matchLine[i*perLine:(i+1)*perLine]
#    print str(i) + ' ',
#    print alignSeq2[i*perLine:(i+1)*perLine]
#print "\nAlignment Score: " + str(matrix[N][M])

if (len(seq1) > len(seq2)):
    print "\nMatch Percentage (Mouse): " + str((matchCtr*1.0) / len(seq1))
else:
    print "\nMatch Percentage (Mouse): " + str((matchCtr*1.0) / len(seq2))
    
    
    

################################# GRAPH ##########################3######

animals = animal

#y_pos = animals
similarity = seqList

#print animals
#print similarity


test = (91.74, 73.44, 66.88)
n_groups = 3
index = np.arange(n_groups)
bar_width = 0.4
opacity=0.4

plt.bar(index, test, bar_width, alpha=opacity, color='g')

plt.xlabel('Types of Animals')
plt.ylabel('Percent Similarity')
plt.title('Percent Similarity of Chimpanzee, Horse, and Mouse to Humans')
plt.xticks(index + bar_width/2, ('Chimpanzee', 'Horse', 'Mouse'))
plt.legend()

plt.tight_layout()
plt.show()





