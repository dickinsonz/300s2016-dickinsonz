from Bio.Blast import NCBIWWW # import the module related to BLAST
from Bio import SeqIO
import matplotlib.pyplot as plt
import numpy as np

my_file = open('sequences.fasta')
count = 0
name = ""
percent = ""
seqList=[]
animal = []

# parse through the whole file and save all seq in a list
for line in my_file:
	if count%2==0:
		name=line[1:len(line)-1]
		print "Name: ", name
		animal.append(name)
	else:
		percent = line.upper()
		print "Percent Similarity: ", percent
		seqList.append(percent)
	count+=1


my_file.close()

############ Needman-Wunsch ################



############ GRAPH #############

animals = animal

#y_pos = animals
similarity = seqList

#listT= [1,2,3]

#print len(animals)
#print len(similarity)
print animals
print similarity



#pylab.bar(listT, similarity, align = 'center')
#pylab.xticks(listT, animals)
#pylab.ylabel('Percent Similarity')
#pylab.xlabel('Types')
#pylab.title('Percent Similarity between Humans and Chimpanzees, Horses, and Mice.')

test = ('','','')
n_groups = 3

index = np.arange(n_groups)
bar_width = 0.4
opacity=0.4

rects= plt.bar(index, test, bar_width, alpha=opacity, color='r',)

plt.xlabel('Types of Animals')
plt.ylabel('Percent Similarity')
plt.title('Title omg')
plt.xticks(index + bar_width/2, ('Chimpanzee', 'Horse', 'Mouse'))
plt.legend()

plt.tight_layout()
plt.show()





