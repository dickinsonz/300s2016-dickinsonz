#*************************************
# Honor Code: This work is mine unless otherwise cited.
# Janyl Jumadinova
# CMPSC 300 Spring 2016
# Class Example
# Date: February 9, 2016

# Purpose: illustrating reading files
#*************************************

# Read an input from a fasta file and print out its name and sequence

my_file = open('Diabetes.fasta')
count = 0
name = ""
sequence = ""
seqList = []

# parse through whole file and save all sequences in a list
for line in my_file:
    if count%2==0:
        name = line[1:len(line)]
        print "Name :", name
        count = count +1
    else:
        sequence = line.upper()
        print "Sequence: ", sequence
        seqList.append(sequence)
        count = count +1


my_file.close()




